-- Table: mockapiamadeus

-- DROP TABLE mockapiamadeus;

CREATE TABLE mockapiamadeus
(
  databusca timestamp without time zone,
  dataviagem timestamp without time zone,
  destino character varying(255),
  link character varying(255),
  origem character varying(255),
  preco double precision,
  site character varying(255),
  id serial NOT NULL,
  CONSTRAINT mockapiamadeus_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE mockapiamadeus
  OWNER TO postgres;


--Preços GOIÂNIA > LONDRES
INSERT INTO mockapiamadeus(
            databusca, dataviagem, destino, link, origem, preco, site)
    VALUES ('20180205', '20180622', 'LONDRES', 'http://amadeusApi.com', 'GOIÂNIA', 820, 'amadeus');

INSERT INTO mockapiamadeus(
            databusca, dataviagem, destino, link, origem, preco, site)
    VALUES ('20180205', '20180618', 'LONDRES', 'http://www.aero.com', 'GOIÂNIA', 690, 'Aero');

INSERT INTO mockapiamadeus(
            databusca, dataviagem, destino, link, origem, preco, site)
    VALUES ('20180205', '20180616', 'LONDRES', 'http://www.azul.com', 'GOIÂNIA', 710, 'Azul');

INSERT INTO mockapiamadeus(
            databusca, dataviagem, destino, link, origem, preco, site)
    VALUES ('20180205', '20180608', 'LONDRES', 'http://www.aero.com', 'GOIÂNIA', 800, 'Aero');



--Preços GOIÂNIA > PARIS
INSERT INTO mockapiamadeus(
        databusca, dataviagem, destino, link, origem, preco, site)
VALUES ('20180205', '20180603', 'PARIS', 'http://www.aero.com', 'GOIÂNIA', 980, 'Aero');

INSERT INTO mockapiamadeus(
        databusca, dataviagem, destino, link, origem, preco, site)
VALUES ('20180205', '20180618', 'PARIS', 'http://www.azul.com', 'GOIÂNIA', 900, 'Azul');

INSERT INTO mockapiamadeus(
        databusca, dataviagem, destino, link, origem, preco, site)
VALUES ('20180205', '20180620', 'PARIS', 'http://www.azul.com', 'GOIÂNIA', 920, 'Azul');


INSERT INTO mockapiamadeus(
        databusca, dataviagem, destino, link, origem, preco, site)
VALUES ('20180205', '20180622', 'PARIS', 'http://www.gol.com', 'GOIÂNIA', 926, 'Gol');

INSERT INTO mockapiamadeus(
        databusca, dataviagem, destino, link, origem, preco, site)
VALUES ('20180205', '20180622', 'PARIS', 'http://www.bluesky.com', 'GOIÂNIA', 975, 'BlueSky');



--Precos GOIÂNIA MOSCOU
INSERT INTO mockapiamadeus(
        databusca, dataviagem, destino, link, origem, preco, site)
VALUES ('20180205', '20180622', 'MOSCOU', 'http://www.azul.com', 'GOIÂNIA', 1022, 'Azul');

INSERT INTO mockapiamadeus(
        databusca, dataviagem, destino, link, origem, preco, site)
VALUES ('20180205', '20180626', 'MOSCOU', 'http://www.azul.com', 'GOIÂNIA', 1000, 'Azul');

INSERT INTO mockapiamadeus(
        databusca, dataviagem, destino, link, origem, preco, site)
VALUES ('20180205', '20180620', 'MOSCOU', 'http://www.azul.com', 'GOIÂNIA', 980, 'Azul');

INSERT INTO mockapiamadeus(
        databusca, dataviagem, destino, link, origem, preco, site)
VALUES ('20180205', '20180621', 'MOSCOU', 'http://www.azul.com', 'GOIÂNIA', 1010, 'Azul');




--Preço Goiania Buenos Aires
INSERT INTO mockapiamadeus(
        databusca, dataviagem, destino, link, origem, preco, site)
VALUES ('20180205', '20180622', 'BUENO AIRES', 'http://www.azul.com', 'GOIÂNIA', 620, 'Azul');

INSERT INTO mockapiamadeus(
        databusca, dataviagem, destino, link, origem, preco, site)
VALUES ('20180205', '20180622', 'BUENO AIRES', 'http://www.gol.com', 'GOIÂNIA', 800, 'Gol');

INSERT INTO mockapiamadeus(
        databusca, dataviagem, destino, link, origem, preco, site)
VALUES ('20180205', '20180622', 'BUENO AIRES', 'http://www.bluesky.com', 'GOIÂNIA', 732, 'BlueSky');

INSERT INTO mockapiamadeus(
        databusca, dataviagem, destino, link, origem, preco, site)
VALUES ('20180205', '20180622', 'BUENO AIRES', 'http://www.masson.com', 'GOIÂNIA', 790, 'Masson');

--Preços GOIÂNIA Rio
INSERT INTO mockapiamadeus(
        databusca, dataviagem, destino, link, origem, preco, site)
VALUES ('20180205', '20180622', 'RIO DE JANEIRO', 'http://www.bluesky.com', 'GOIÂNIA', 380, 'BlueSky');

INSERT INTO mockapiamadeus(
        databusca, dataviagem, destino, link, origem, preco, site)
VALUES ('20180205', '20180626', 'RIO DE JANEIRO', 'http://www.masson.com', 'GOIÂNIA', 390, 'Masson');

INSERT INTO mockapiamadeus(
        databusca, dataviagem, destino, link, origem, preco, site)
VALUES ('20180205', '20180618', 'RIO DE JANEIRO', 'http://www.gol.com', 'GOIÂNIA', 328, 'Gol');

INSERT INTO mockapiamadeus(
        databusca, dataviagem, destino, link, origem, preco, site)
VALUES ('20180205', '20180617', 'RIO DE JANEIRO', 'http://www.gol.com', 'GOIÂNIA', 328, 'Gol');

INSERT INTO mockapiamadeus(
        databusca, dataviagem, destino, link, origem, preco, site)
VALUES ('20180205', '20180606', 'RIO DE JANEIRO', 'http://www.azul.com', 'GOIÂNIA', 400, 'Azul');

INSERT INTO mockapiamadeus(
        databusca, dataviagem, destino, link, origem, preco, site)
VALUES ('20180205', '20180621', 'RIO DE JANEIRO', 'http://www.azul.com', 'GOIÂNIA', 400, 'Azul');

INSERT INTO mockapiamadeus(
        databusca, dataviagem, destino, link, origem, preco, site)
VALUES ('20180205', '20180615', 'RIO DE JANEIRO', 'http://www.azul.com', 'GOIÂNIA', 400, 'Azul');


--Outros
INSERT INTO mockapiamadeus(
        databusca, dataviagem, destino, link, origem, preco, site)
VALUES ('20180205', '20180622', 'ACRE', 'http://www.masson.com', 'GOIÂNIA', 260, 'Masson');

INSERT INTO mockapiamadeus(
        databusca, dataviagem, destino, link, origem, preco, site)
VALUES ('20180205', '20180622', 'MANAUS', 'http://www.gol.com', 'GOIÂNIA', 280, 'Gol');