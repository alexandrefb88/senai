﻿-- Table: usuarios

-- DROP TABLE usuarios;

CREATE TABLE usuarios
(
  id integer NOT NULL,
  email character varying(255),
  idcidade integer NOT NULL,
  lembretesenha character varying(255),
  nome character varying(255),
  senha character varying(255),
  CONSTRAINT usuarios_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE usuarios
  OWNER TO postgres;
