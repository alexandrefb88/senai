﻿-- Table: viagens

-- DROP TABLE viagens;

CREATE TABLE viagens
(
  id integer NOT NULL,
  databusca timestamp without time zone,
  dataviagem timestamp without time zone,
  destino character varying(255),
  idusuario integer NOT NULL,
  link character varying(255),
  origem character varying(255),
  preco double precision,
  site character varying(255),
  CONSTRAINT viagens_pkey PRIMARY KEY (id)
)