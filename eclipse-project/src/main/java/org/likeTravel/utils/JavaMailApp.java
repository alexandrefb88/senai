package org.likeTravel.utils;

import java.util.Properties;
import javax.mail.Address;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class JavaMailApp {
	//ativar segurança baixa da conta de email: https://myaccount.google.com/lesssecureapps?pli=1
	private static String SMTP_EMAIL_AUT = "liketravelmail@gmail.com";
	private static String SMTP_PASS = "vfgod400";
	
	public static void enviarEmail(String destinatario, String assunto, String texto) {
		Properties props = new Properties();
		/** Parâmetros de conexão com servidor Gmail */
		props.put("mail.smtp.host", "smtp.gmail.com");
		props.put("mail.smtp.socketFactory.port", "465");
		props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.port", "465");

		Session session = Session.getDefaultInstance(props, new javax.mail.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(SMTP_EMAIL_AUT, SMTP_PASS);
			}
		});

		/** Ativa Debug para sessão */
		session.setDebug(true);

		try {

			MimeMessage message = new MimeMessage(session);
			
			message.setFrom(new InternetAddress(SMTP_EMAIL_AUT)); // Remetente

			Address[] toUser = InternetAddress.parse(destinatario);

			message.setRecipients(Message.RecipientType.TO, toUser);
			message.setSubject(assunto);
			message.setText(texto,"utf-8","html");
			message.setContent(texto,"text/html; charset=utf-8");
			/** Método para enviar a mensagem criada */
			Transport.send(message);

		} catch (MessagingException e) {
			throw new RuntimeException(e);
		}
	}
	
	public static String montarHtmlRecuperacaoSenha(String email, String senha) {
		StringBuilder html = new StringBuilder();
		html.append("<html>");
		html.append("<head>");
		html.append("<style>");
		html.append("table {");
		html.append("    font-family: arial, sans-serif;");
		html.append("    border-collapse: collapse;");
		html.append("    width: 100%;");
		html.append("}");
		html.append("");
		html.append("td, th {");
		html.append("    border: 1px solid #dddddd;");
		html.append("    text-align: left;");
		html.append("    padding: 8px;");
		html.append("}");
		html.append("tr:nth-child(even) {");
		html.append("    background-color: #dddddd;");
		html.append("}");
		html.append("</style>");
		html.append("</head>");
		html.append("<body>");
		html.append("<img src='https://dl.dropboxusercontent.com/s/odhyltzmb4sly5e/banner.png?dl=0' width='335px' height='83px'/>");
		html.append("<h3>Use os dados abaixo para logar</h3>");
		html.append("<table style='width:335px'>");
		html.append("  <tr>");
		html.append("    <th>Email:</th>");
		html.append("    <th>Senha:</th>");
		html.append("  </tr>");
		html.append("  <tr>");
		html.append("    <td>@param_email</td>");
		html.append("    <td>@param_senha</td>");
		html.append("  </tr>");
		html.append("  </table>");
		html.append("</body>");
		html.append("</html>");
		
		return html.toString().replaceAll("@param_email", email).replaceAll("@param_senha", senha);
	}
}
