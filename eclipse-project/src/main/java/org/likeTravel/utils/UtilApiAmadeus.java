package org.likeTravel.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.xml.bind.JAXBException;

import org.likeTravel.model.ViagemDTO;
import org.primefaces.json.JSONArray;
import org.primefaces.json.JSONException;
import org.primefaces.json.JSONObject;

public class UtilApiAmadeus implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -3232823770656760415L;
	private static String AMADEUS_API_KEY = "BfB9olbkUIJH1jAdTPvqbR0dkztAOGG8";
	private static String AMADEUS_API_URL_BASE = "https://api.sandbox.amadeus.com/v1.2/flights/inspiration-search?apikey=";

	public static String montarUrlPesquisa(String origem, String destino, Date dataViagem, int duracao,
			int precoMaximo) {
		return AMADEUS_API_URL_BASE + AMADEUS_API_KEY + "&origin=" + origem + "&destination=" + destino
				+ "&departure_date=" + UtilFaces.formatarData(dataViagem, "yyyy-MM-dd") /*+ "&duration="
				+ String.valueOf(duracao) */+ "&maxprice=" + precoMaximo;
	}

	public static List<ViagemDTO> consultarApi(String urlApi) throws JAXBException, JSONException, ParseException {
		try {
			URL url = new URL(urlApi);
			HttpURLConnection con = (HttpURLConnection) url.openConnection();

			if (con.getResponseCode() != 200) {
				throw new RuntimeException("HTTP error code : " + con.getResponseCode());
			}

			BufferedReader br = new BufferedReader(new InputStreamReader((con.getInputStream())));
			String json = getJsonDoBufferedReader(br);
			con.disconnect();

			return transformarJsonLista(json);

		} catch (MalformedURLException e1) {
			e1.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	private static String getJsonDoBufferedReader(BufferedReader br) throws IOException {
		StringBuilder json = new StringBuilder();
		String linha = br.readLine();
		while (linha != null) {
			json.append(linha);
			linha = br.readLine();
		}
		return json.toString();
	}

	private static List<ViagemDTO> transformarJsonLista(String strJson) throws JSONException, ParseException {
		List<ViagemDTO> lista = new ArrayList<>();
		JSONObject json = new JSONObject(strJson);
		JSONArray jsonArray = json.getJSONArray("results");
		for (int i = 0; i < jsonArray.length(); i++) {
			JSONObject jsonObj = jsonArray.getJSONObject(i);
			ViagemDTO viagem = new ViagemDTO();
			viagem.setDestino(jsonObj.getString("destination"));
			viagem.setDataSaida(UtilFaces.getDataDaString((jsonObj.getString("departure_date"))));
			viagem.setLinhaAerea(jsonObj.getString("airline"));
			viagem.setPreco(jsonObj.getString("price"));
			lista.add(viagem);
		}
		return lista;
	}

}
