package org.likeTravel.entidades;

import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.likeTravel.utils.BaseEntities;

@Entity
@Table(name = "Estado")
@AttributeOverride(name="id", column = @Column(name = "id"))
public class Estado extends BaseEntities<Integer>{
	private static final long serialVersionUID = 1L;

	private int codigoUf;
	private String nome;
	private String uf;
	private int regiao;
	
	public Estado() {
		
	}
	public Estado(int id,int codigoUf,String nome, String uf, int regiao) {
		this.codigoUf = codigoUf;
		this.nome = nome;
		this.uf = uf;
		this.regiao = regiao;
	}
	public int getCodigoUf() {
		return codigoUf;
	}
	public void setCodigoUf(int codigoUf) {
		this.codigoUf = codigoUf;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getUf() {
		return uf;
	}
	public void setUf(String uf) {
		this.uf = uf;
	}
	public int getRegiao() {
		return regiao;
	}
	public void setRegiao(int regiao) {
		this.regiao = regiao;
	}
}
