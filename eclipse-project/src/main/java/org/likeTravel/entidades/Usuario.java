package org.likeTravel.entidades;

import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.likeTravel.utils.BaseEntities;
@Entity
@Table(name = "USUARIOS")
@AttributeOverride(name="id", column = @Column(name = "id"))
public class Usuario extends BaseEntities<Integer>{
	private static final long serialVersionUID = 1L;
	private String nome;
	private String email;
	private String senha;
	private String lembreteSenha;
	private int idCidade;
	
	public Usuario() {
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getSenha() {
		return senha;
	}
	public void setSenha(String senha) {
		this.senha = senha;
	}
	public String getLembreteSenha() {
		return lembreteSenha;
	}
	public void setLembreteSenha(String lembreteSenha) {
		this.lembreteSenha = lembreteSenha;
	}
	public int getCidade() {
		return idCidade;
	}
	public void setCidade(int idCidade) {
		this.idCidade = idCidade;
	}
}
