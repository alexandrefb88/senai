package org.likeTravel.entidades;

import java.util.Date;

import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.likeTravel.utils.BaseEntities;

@Entity
@Table(name = "VIAGENS")
@AttributeOverride(name="id", column = @Column(name = "id"))
public class Viagem extends BaseEntities<Integer>{
	private static final long serialVersionUID = 1L;
	
	public Viagem(int idUsuario, String site, String link, Date dataViagem, String origem, String destino, Double preco,
			Date dataBusca) {
		super();
		this.idUsuario = idUsuario;
		this.site = site;
		this.link = link;
		this.dataViagem = dataViagem;
		this.origem = origem;
		this.destino = destino;
		this.preco = preco;
		this.dataBusca = dataBusca;
	}
	
	public Viagem() {
		
	}
	private int idUsuario;
	private String site;
	private String link;
	private Date dataViagem;
	private String origem;
	private String destino;
	private Double preco;
	private Date dataBusca;
	
	public int getIdUsuario() {
		return idUsuario;
	}
	public void setIdUsuario(int idUsuario) {
		this.idUsuario = idUsuario;
	}
	public String getSite() {
		return site;
	}
	public void setSite(String site) {
		this.site = site;
	}
	public String getLink() {
		return link;
	}
	public void setLink(String link) {
		this.link = link;
	}
	public Date getDataViagem() {
		return dataViagem;
	}
	public void setDataViagem(Date dataViagem) {
		this.dataViagem = dataViagem;
	}
	public String getOrigem() {
		return origem;
	}
	public void setOrigem(String origem) {
		this.origem = origem;
	}
	public String getDestino() {
		return destino;
	}
	public void setDestino(String destino) {
		this.destino = destino;
	}
	public Double getPreco() {
		return preco;
	}
	public void setPreco(Double preco) {
		this.preco = preco;
	}
	public Date getDataBusca() {
		return dataBusca;
	}
	public void setDataBusca(Date dataBusca) {
		this.dataBusca = dataBusca;
	}
	
	public boolean equals(Viagem obj) {
		if(obj == null) {
			return false;
		}
		return obj.getSite().equals(this.site) 
				&& obj.getLink().equals(this.link)
				&& obj.getDataViagem().equals(this.dataViagem)
				&& obj.getOrigem().equals(this.origem)
				&& obj.getDestino().equals(this.destino)
				&& obj.getPreco().equals(this.preco);
	}
}
