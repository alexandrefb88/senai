package org.likeTravel.entidades;

import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.likeTravel.utils.BaseEntities;
@Entity
@Table(name = "Municipio")
@AttributeOverride(name="id", column = @Column(name = "id"))
public class Municipio extends BaseEntities<Integer>{

	private static final long serialVersionUID = 1L;
	private int codigo;
	private String nome;
	private String uf;
	
	public Municipio(int id, int codigo, String nome, String uf) {
		super();
		this.codigo = codigo;
		this.nome = nome;
		this.uf = uf;
	}
	public Municipio() {
	}
	
	public int getCodigo() {
		return codigo;
	}
	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getUf() {
		return uf;
	}
	public void setUf(String uf) {
		this.uf = uf;
	}
}
