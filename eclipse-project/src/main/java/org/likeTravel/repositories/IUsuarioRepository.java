package org.likeTravel.repositories;

import org.likeTravel.entidades.Usuario;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IUsuarioRepository extends JpaRepository<Usuario, Integer>{
	
	Usuario findByEmailAndSenha(String email,String senha);
	
	Usuario findByEmail(String email);
}
