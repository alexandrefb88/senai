package org.likeTravel.repositories;

import org.likeTravel.entidades.Estado;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IEstadoRepository extends JpaRepository<Estado, Integer>{
	Estado findByUf(String uf);
}
