package org.likeTravel.repositories;


import java.util.List;

import org.likeTravel.entidades.Municipio;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface IMunicipioRepository extends JpaRepository<Municipio, Integer>{
	public final static String OBTER_POR_ESTADO = "SELECT m FROM Municipio m WHERE uf = :uf";
	
	@Query(OBTER_POR_ESTADO)
	List<Municipio> findByUf(@Param("uf") String uf);
}
