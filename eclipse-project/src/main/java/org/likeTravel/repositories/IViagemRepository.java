package org.likeTravel.repositories;

import java.util.List;

import org.likeTravel.entidades.Viagem;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IViagemRepository extends JpaRepository<Viagem, Integer>{
	
	List<Viagem> findByIdUsuario(int usuario);
}
