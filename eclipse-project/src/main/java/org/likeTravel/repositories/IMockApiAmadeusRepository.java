package org.likeTravel.repositories;

import java.util.Date;
import java.util.List;

import org.likeTravel.entidades.MockApiAmadeus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface IMockApiAmadeusRepository extends JpaRepository<MockApiAmadeus, Integer> {
	
	@Query("SELECT M FROM MockApiAmadeus M "
			+ "WHERE origem = :origem "
			+ "AND destino = :destino "
			+ "AND dataViagem BETWEEN :dataInicio AND :dataFim "
			+ "ORDER BY preco ASC")
	List<MockApiAmadeus> findByOrigemAndDestino(@Param("origem")String origem, @Param("destino")String destino, 
			@Param("dataInicio")Date dataInicio, @Param("dataFim")Date dataFim);
}
