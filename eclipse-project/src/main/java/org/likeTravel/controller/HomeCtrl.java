package org.likeTravel.controller;

import javax.inject.Inject;
import javax.inject.Named;

import org.likeTravel.entidades.Usuario;
import org.likeTravel.repositories.IUsuarioRepository;
import org.likeTravel.utils.BaseBeans;
import org.springframework.context.annotation.Scope;
import org.springframework.web.context.WebApplicationContext;

@Scope(value = WebApplicationContext.SCOPE_SESSION)
@Named(value = "homeCtrl")
public class HomeCtrl extends BaseBeans {

	private static final long serialVersionUID = 1L;
	private Usuario usuarioLogado;
	@Inject
	private IUsuarioRepository usuarioRepository;
	
	public void carregarUsuarioLogado() {
			usuarioLogado = usuarioRepository.findByEmail(obterEmailUsuarioLogado());
	}
	
	public Usuario getUsuarioLogado() {
		return usuarioLogado;
	}
	public void setUsuarioLogado(Usuario usuarioLogado) {
		this.usuarioLogado = usuarioLogado;
	}

}
