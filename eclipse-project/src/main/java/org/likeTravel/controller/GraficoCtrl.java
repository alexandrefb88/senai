package org.likeTravel.controller;

import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Named;

import org.likeTravel.entidades.MockApiAmadeus;
import org.likeTravel.repositories.IMockApiAmadeusRepository;
import org.likeTravel.utils.BaseBeans;
import org.primefaces.model.chart.PieChartModel;
import org.springframework.context.annotation.Scope;
import org.springframework.web.context.WebApplicationContext;

@Scope(value = WebApplicationContext.SCOPE_SESSION)
@Named(value = "graficoCtrl")
public class GraficoCtrl extends BaseBeans {
	private static final long serialVersionUID = -3859784018197155780L;
	
	private Date dataInicioPesquisa;
	private Date dataFimPesquisa;
	private String cidadeOrigem;
	private String cidadeDestino;
	private PieChartModel graficoPizza;
	@Inject
	private IMockApiAmadeusRepository apiAmadeusRepository;
	
	@PostConstruct
	public void init() {
		graficoPizza = new PieChartModel();
		graficoPizza.set("Sem Registro", 100);
		graficoPizza.setTitle("Resultado da Pesquisa");
		graficoPizza.setLegendPosition("w");
	}
	
	public PieChartModel getGraficoPizza() {
		return graficoPizza;
	}

	public void setGraficoPizza(PieChartModel graficoPizza) {
		this.graficoPizza = graficoPizza;
	}

	public void montarGrafico() {
		List<MockApiAmadeus> lista = apiAmadeusRepository.findByOrigemAndDestino(cidadeOrigem.toUpperCase(), cidadeDestino.toUpperCase(), 
				dataInicioPesquisa, dataFimPesquisa);
		graficoPizza = new PieChartModel();
		graficoPizza.setTitle("Resultado da Pesquisa");
		graficoPizza.setLegendPosition("w");
		for(MockApiAmadeus api: lista) {
			graficoPizza.set(api.getSite(), api.getPreco());
		}
		
	}
	
	public Date getDataInicioPesquisa() {
		return dataInicioPesquisa;
	}
	public void setDataInicioPesquisa(Date dataInicioPesquisa) {
		this.dataInicioPesquisa = dataInicioPesquisa;
	}
	public Date getDataFimPesquisa() {
		return dataFimPesquisa;
	}
	public void setDataFimPesquisa(Date dataFimPesquisa) {
		this.dataFimPesquisa = dataFimPesquisa;
	}
	public String getCidadeOrigem() {
		return cidadeOrigem;
	}
	public void setCidadeOrigem(String cidadeOrigem) {
		this.cidadeOrigem = cidadeOrigem;
	}
	public String getCidadeDestino() {
		return cidadeDestino;
	}
	public void setCidadeDestino(String cidadeDestino) {
		this.cidadeDestino = cidadeDestino;
	}

}
