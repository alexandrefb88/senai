package org.likeTravel.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;
import javax.xml.bind.JAXBException;

import org.likeTravel.entidades.MockApiAmadeus;
import org.likeTravel.entidades.Usuario;
import org.likeTravel.entidades.Viagem;
import org.likeTravel.model.ViagemDTO;
import org.likeTravel.repositories.IMockApiAmadeusRepository;
import org.likeTravel.repositories.IUsuarioRepository;
import org.likeTravel.repositories.IViagemRepository;
import org.likeTravel.utils.BaseBeans;
import org.likeTravel.utils.UtilApiAmadeus;
import org.likeTravel.utils.UtilFaces;
import org.springframework.context.annotation.Scope;
import org.springframework.expression.ParseException;
import org.springframework.web.context.WebApplicationContext;

@Scope(value = WebApplicationContext.SCOPE_SESSION)
@Named(value = "viagemCtrl")
public class ViagemCtrl extends BaseBeans {
	private static final long serialVersionUID = 1L;
	public static String AMADEUS_API_KEY = "BfB9olbkUIJH1jAdTPvqbR0dkztAOGG8";

	@Inject
	private IViagemRepository viagemRepository;
	@Inject
	private IMockApiAmadeusRepository apiAmadeusRepository;
	@Inject
	private IUsuarioRepository usuarioRepository;

	private List<Viagem> listaViagens;
	private List<Viagem> listaFavoritos;
	private List<ViagemDTO> listaAmadeus;
	private Date dataInicioPesquisa;
	private Date dataFimPesquisa;
	private String cidadeOrigem;
	private String cidadeDestino;

	public void carregarFavoritos() {
		String email = this.obterEmailUsuarioLogado();
		Usuario usuario = usuarioRepository.findByEmail(email);
		listaFavoritos = viagemRepository.findByIdUsuario(usuario.getId());
	}

	public void pesquisarViagem() {
		listaViagens = new ArrayList<>();
		List<MockApiAmadeus> results = apiAmadeusRepository.findByOrigemAndDestino(cidadeOrigem.toUpperCase(),
				cidadeDestino.toUpperCase(), dataInicioPesquisa, dataFimPesquisa);
		for (MockApiAmadeus m : results) {
			Viagem v = new Viagem();
			v.setDataBusca(m.getDataBusca());
			v.setDataViagem(m.getDataViagem());
			v.setDestino(m.getDestino());
			v.setLink(m.getLink());
			v.setOrigem(m.getOrigem());
			v.setPreco(m.getPreco());
			v.setSite(m.getSite());
			listaViagens.add(v);
		}
	}

	public void pesquisarViagemApi() throws ParseException, java.text.ParseException {
		listaAmadeus = new ArrayList<>();
		if(dataInicioPesquisa.before(new Date())){
			UtilFaces.exibirMensagemSucesso("Data da pesquisa deve ser maior que a data atual");
			return ;
		}
		String url = UtilApiAmadeus.montarUrlPesquisa(cidadeOrigem, "", dataInicioPesquisa, 5, 1000);
		try {
			listaAmadeus = UtilApiAmadeus.consultarApi(url);
		} catch (JAXBException e) {
			e.printStackTrace();
		}
	}

	public void favoritar(Viagem viagem) {
		String email = this.obterEmailUsuarioLogado();
		Usuario usuario = usuarioRepository.findByEmail(email);
		if (usuario != null) {
			viagem.setIdUsuario(usuario.getId());
			viagemRepository.save(viagem);
			UtilFaces.exibirMensagemSucesso("Item adicionado aos favoritos.");
		} else {
			UtilFaces.exibirMensagemSucesso("É preciso estar logado para usar esta função.");
		}
	}

	public void removerFavorito(Viagem viagem) {
		viagemRepository.delete(viagem.getId());
	}

	public List<Viagem> getListaViagens() {
		return listaViagens;
	}

	public void setListaViagens(List<Viagem> listaViagens) {
		this.listaViagens = listaViagens;
	}

	public Date getDataInicioPesquisa() {
		return dataInicioPesquisa;
	}

	public void setDataInicioPesquisa(Date dataInicioPesquisa) {
		this.dataInicioPesquisa = dataInicioPesquisa;
	}

	public Date getDataFimPesquisa() {
		return dataFimPesquisa;
	}

	public void setDataFimPesquisa(Date dataFimPesquisa) {
		this.dataFimPesquisa = dataFimPesquisa;
	}

	public String getCidadeOrigem() {
		return cidadeOrigem;
	}

	public void setCidadeOrigem(String cidadeOrigem) {
		this.cidadeOrigem = cidadeOrigem;
	}

	public String getCidadeDestino() {
		return cidadeDestino;
	}

	public void setCidadeDestino(String cidadeDestino) {
		this.cidadeDestino = cidadeDestino;
	}

	public List<Viagem> getListaFavoritos() {
		return listaFavoritos;
	}

	public void setListaFavoritos(List<Viagem> listaFavoritos) {
		this.listaFavoritos = listaFavoritos;
	}
	
	public List<ViagemDTO> getListaAmadeus() {
		return listaAmadeus;
	}

	public void setListaAmadeus(List<ViagemDTO> listaAmadeus) {
		this.listaAmadeus = listaAmadeus;
	}

}
