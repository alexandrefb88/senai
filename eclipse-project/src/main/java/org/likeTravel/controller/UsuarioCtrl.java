package org.likeTravel.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.faces.event.AjaxBehaviorEvent;
import javax.inject.Inject;
import javax.inject.Named;

import org.likeTravel.entidades.Estado;
import org.likeTravel.entidades.Municipio;
import org.likeTravel.entidades.Usuario;
import org.likeTravel.repositories.IEstadoRepository;
import org.likeTravel.repositories.IMunicipioRepository;
import org.likeTravel.repositories.IUsuarioRepository;
import org.likeTravel.utils.BaseBeans;
import org.likeTravel.utils.JavaMailApp;
import org.likeTravel.utils.UtilFaces;
import org.springframework.context.annotation.Scope;
import org.springframework.util.StringUtils;
import org.springframework.web.context.WebApplicationContext;

@Scope(value = WebApplicationContext.SCOPE_SESSION)
@Named(value = "usuarioCtrl")
public class UsuarioCtrl extends BaseBeans {
	private static final long serialVersionUID = 1L;
	private Usuario usuario;
	private List<Estado> listaEstados;
	private String estadoSelecionado;
	private List<Municipio> listaCidade;
	private int idCidadeSelecionada;
	private String emailRecuperarSenha;

	private Integer id;

	@Inject
	private IEstadoRepository estadoRepository;
	@Inject
	private IMunicipioRepository municipioRepository;
	@Inject
	private IUsuarioRepository usuarioRepository;

	public UsuarioCtrl() {
	}

	public void onLoad() {
		if (usuario == null || (usuario != null && StringUtils.isEmpty(usuario.getEmail()))) {
			inicializarAlteracao();
		}
	}

	public void inicializarCadastro() {
		usuario = new Usuario();
		listaEstados = estadoRepository.findAll();
	}

	public void inicializarAlteracao() {
		usuario = usuarioRepository.findByEmail(obterEmailUsuarioLogado());
		Municipio cidade = municipioRepository.findOne(usuario.getCidade());
		estadoSelecionado = estadoRepository.findByUf(cidade.getUf()).getUf();
		listaCidade = municipioRepository.findByUf(cidade.getUf());
		setIdCidadeSelecionada(cidade.getId());
		listaEstados = estadoRepository.findAll();
	}

	public List<Municipio> getListaCidade() {
		return listaCidade;
	}

	public void setListaCidade(List<Municipio> cidade) {
		this.listaCidade = cidade;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public void setListaEstados(List<Estado> listaEstados) {
		this.listaEstados = listaEstados;
	}

	public List<Estado> getListaEstados() {
		return listaEstados;
	}

	public String getEstadoSelecionado() {
		return estadoSelecionado;
	}

	public void setEstadoSelecionado(String estadoSelecionado) {
		this.estadoSelecionado = estadoSelecionado;
	}

	public void cadastrar() {
		if(usuarioRepository.findByEmail(usuario.getEmail()) == null) {
			String senhaEncriptada = UtilFaces.encriptarSenha(usuario.getSenha());
			usuario.setSenha(senhaEncriptada);
			usuario.setCidade(idCidadeSelecionada);
			usuarioRepository.save(usuario);
			UtilFaces.exibirMensagemSucesso("Dados salvos com sucesso.");
			limparTela();
		} else {
			UtilFaces.exibirMensagemSucesso("Já existe um usuário com este e-mail.");
		}

	}
	
	public void alterar() throws IOException {
		String senhaEncriptada = UtilFaces.encriptarSenha(usuario.getSenha());
		usuario.setSenha(senhaEncriptada);
		usuario.setCidade(idCidadeSelecionada);
		usuarioRepository.save(usuario);
		UtilFaces.exibirMensagemSucesso("Dados salvos com sucesso.");
		UtilFaces.redirect("../home.xhtml");
	}
	
	public void gerarNovaSenha() {
		String novaSenha = UtilFaces.gerarSenhaAleatoria();
		String senhaEncriptada = UtilFaces.encriptarSenha(novaSenha);
		Usuario usuario = usuarioRepository.findByEmail(emailRecuperarSenha);
		if(usuario != null) {
			usuario.setSenha(senhaEncriptada);
			usuarioRepository.save(usuario);
			enviarSenhaPorEmail(novaSenha, emailRecuperarSenha);
			UtilFaces.exibirMensagemSucesso("A nova senha foi gerada e enviada no seu e-mail.");
		}else {
			UtilFaces.exibirMensagemSucesso("O email informado não está cadastrado.");
		}
		
	}
	
	private void enviarSenhaPorEmail(String senha, String email) {
		String conteudo = JavaMailApp.montarHtmlRecuperacaoSenha(email, senha);
		JavaMailApp.enviarEmail(email, "Like Travel Recuperação de senha", conteudo);
	}

	public void limparTela() {
		usuario = null;
		idCidadeSelecionada = 0;
		estadoSelecionado = null;
	}

	public void excluir() throws IOException {
		if (usuario != null) {
			usuarioRepository.delete(usuario.getId());
			UtilFaces.exibirMensagemSucesso("Dados excluídos.");
			UtilFaces.redirect("../../cadastrar.xhtml");
		}
	}

	public void estadoChanged(AjaxBehaviorEvent e) {
		preencherCidadesPorEstado();
	}

	public void cidadeChanged(AjaxBehaviorEvent e) {
		if (idCidadeSelecionada > 0) {
			Municipio cidade = municipioRepository.findOne(idCidadeSelecionada);
			usuario.setCidade(cidade.getId());
		}
	}

	public void preencherCidadesPorEstado() {
		if (!StringUtils.isEmpty(estadoSelecionado)) {
			listaCidade = municipioRepository.findByUf(estadoSelecionado);
		} else {
			listaCidade = new ArrayList<>();
		}
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public int getIdCidadeSelecionada() {
		return idCidadeSelecionada;
	}

	public void setIdCidadeSelecionada(int idCidadeSelecionada) {
		this.idCidadeSelecionada = idCidadeSelecionada;
	}

	public String getEmailRecuperarSenha() {
		return emailRecuperarSenha;
	}

	public void setEmailRecuperarSenha(String emailRecuperarSenha) {
		this.emailRecuperarSenha = emailRecuperarSenha;
	}
}
