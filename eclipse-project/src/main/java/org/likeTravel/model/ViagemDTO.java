package org.likeTravel.model;

import java.util.Date;

public class ViagemDTO {

	private Date dataSaida;
	private String destino;
	private String preco;
	private String linhaAerea;
	
	public Date getDataSaida() {
		return dataSaida;
	}
	public void setDataSaida(Date dataSaida) {
		this.dataSaida = dataSaida;
	}

	public String getPreco() {
		return preco;
	}
	public void setPreco(String preco) {
		this.preco = preco;
	}
	public String getLinhaAerea() {
		return linhaAerea;
	}
	public void setLinhaAerea(String linhaAerea) {
		this.linhaAerea = linhaAerea;
	}
	
	public String getDestino() {
		return destino;
	}
	public void setDestino(String destino) {
		this.destino = destino;
	}
}
