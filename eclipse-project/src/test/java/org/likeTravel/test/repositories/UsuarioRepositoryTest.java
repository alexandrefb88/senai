package org.likeTravel.test.repositories;

import static org.junit.Assert.assertEquals;

import java.util.List;

import javax.inject.Inject;

import org.apache.log4j.Logger;
import org.junit.Test;
import org.likeTravel.entidades.Usuario;
import org.likeTravel.repositories.IUsuarioRepository;
import org.likeTravel.test.AbstractDatabaseTest;

import junit.framework.Assert;

public class UsuarioRepositoryTest extends AbstractDatabaseTest {
	private static final Logger LOGGER = Logger.getLogger(IUsuarioRepository.class);

	@Inject
	private IUsuarioRepository usuarioRepository;

	@Test
	public void findAll_test() {
		List<Usuario> lista = this.usuarioRepository.findAll();
		LOGGER.info(lista);
	}

	@Test
	public void findByEmailAndSenha_naoExisteTest() {
		Usuario user = this.usuarioRepository.findByEmailAndSenha("naoExiste", "123456");
		assertEquals(null, user);
		LOGGER.info(user);
	}

	@Test
	public void findByEmailAndSenha_existeTest() {
		List<Usuario> lista = this.usuarioRepository.findAll();
		if (lista != null && !lista.isEmpty()) {
			Usuario existente = lista.get(0);
			Usuario user = this.usuarioRepository.findByEmailAndSenha(existente.getEmail(), existente.getSenha());
			assertEquals(existente.getEmail(), user.getEmail());
			LOGGER.info(user);
		}
	}
	
	@Test
	public void findByEmailAndSenha_emailExisteMasSenhaErradaTest() {
		List<Usuario> lista = this.usuarioRepository.findAll();
		if (lista != null && !lista.isEmpty()) {
			Usuario existente = lista.get(0);
			Usuario user = this.usuarioRepository.findByEmailAndSenha(existente.getEmail(), "senhaQualquer");
			assertEquals(user, null);
			LOGGER.info(user);
		}
	}
	
	@Test
	public void findByEmail_test() {
		List<Usuario> lista = this.usuarioRepository.findAll();
		if (lista != null && !lista.isEmpty()) {
			Usuario existente = lista.get(0);
			Usuario user = this.usuarioRepository.findByEmail(existente.getEmail());
			assertEquals(existente.getEmail(), user.getEmail(),user.getEmail());
			LOGGER.info(user);
		}
	}
}
