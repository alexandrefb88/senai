package org.likeTravel.test.repositories;

import static org.junit.Assert.assertEquals;

import java.util.List;

import javax.inject.Inject;

import org.apache.log4j.Logger;
import org.junit.Test;
import org.likeTravel.entidades.Viagem;
import org.likeTravel.repositories.IViagemRepository;
import org.likeTravel.test.AbstractDatabaseTest;

public class ViagemRepositoryTest extends AbstractDatabaseTest{
	private static final Logger LOGGER = Logger.getLogger(IViagemRepository.class);

	@Inject
	IViagemRepository viagemRepository;

	
	@Test
	public void findAll_test() {
		List<Viagem> lista = this.viagemRepository.findAll();
		LOGGER.info(lista);
	}
	
	@Test
	public void findByIdUsuario_test() {
		List<Viagem> lista = this.viagemRepository.findAll();
		if(lista != null && !lista.isEmpty()) {
			List<Viagem> favoritos = this.viagemRepository.findByIdUsuario(lista.get(0).getIdUsuario());
			assertEquals(favoritos.get(0).getIdUsuario(), lista.get(0).getIdUsuario(),lista.get(0).getIdUsuario());
		}
	}
}
